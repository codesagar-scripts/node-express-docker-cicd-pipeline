const request = require('supertest');
const { app, server } = require('../app');

afterAll((done) => {
    server.close(done);
});

test('should return CodeSagar API', function(done) {
    request(app)
        .get('/')
        .expect(200)
        .expect('CodeSagar API')
        .end(done);
});

